<?php
/**
 * @file
 * Forms file for moneybird commerce module.
 * Defines the content of the forms included in the module
 * Written by Kevin Muller (kevin@enova-tech.net),
 * Cedric Alfonsi (cedric.alfonsi@enova-tech.net)
 */

/**
 * Get the settings form for the module.
 *
 * The template part is buily dynamically from MoneyBird.
 * So if Moneybird is not accessible, an empty field is shown.
 */
function moneybird_commerce_settings_form_content() {
  $form['moneybird_commerce'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['moneybird_commerce']['moneybird_commerce_client'] = array(
    '#type' => 'textfield',
    '#title' => t('Subdomain'),
    '#default_value' => variable_get('moneybird_commerce_client', ''),
    '#size' => 60,
    '#maxlength' => 64,
    '#description' => t('Please enter your moneybird subdomain.'),
  );

  $form['moneybird_commerce']['moneybird_commerce_emailaddress'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#default_value' => variable_get('moneybird_commerce_emailaddress', ''),
    '#size' => 60,
    '#maxlength' => 64,
    '#description' => t('Please enter your moneybird account emailaddress'),
  );

  $form['moneybird_commerce']['moneybird_commerce_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('moneybird_commerce_password', ''),
    '#size' => 60,
    '#maxlength' => 64,
    '#description' => t('Please enter your Moneybird password'),
  );

  // The labels where the wrong way around. They are now changed.
  $form['moneybird_commerce']['moneybird_commerce_price_incl_taxes'] = array(
    '#type' => 'select',
    '#title' => t('Are the taxes already included in the prices sent to MoneyBird ?'),
    '#options' => array(
      '0' => t('No'),
      '1' => t('Yes'),
    ),
    '#default_value' => variable_get('moneybird_commerce_price_incl_taxes', 0),
    '#description' => t(
      'Select No if you do not wish MoneyBird to add taxes to the total price.'
    ),
  );

  try{
    // Get the list of available Invoice Profiles from MoneyBird.
    $connector = _moneybird_commerce_init_moneybird_connection();
    $invoice_profile_service = $connector->getService('InvoiceProfile');
    $profiles = $invoice_profile_service->getAll();

    // Prepare the options array.
    $profile_options = array();
    foreach ($profiles as $p) {
      $profile_options[$p->id] = $p->name;
    }
    // Create the select field for invoice profiles.
    $form['moneybird_commerce']['moneybird_commerce_template'] = array(
      '#type' => 'select',
      '#title' => t('Which invoice template do you want to use'),
      '#options' => $profile_options,
      '#default_value' => variable_get('moneybird_commerce_template'),
      '#description' => t(
        'Select a template for your invoice. The language used in your invoices is also dependent on the profiles settings that you set in MoneyBird.'
      ),
    );
  } catch(Exception $e){
    // We reach here if we were not able to connect to MoneyBird.
    // In this case, we display an empty field and the explanations.
    $form['moneybird_commerce']['moneybird_commerce_template'] = array(
      '#type' => 'select',
      '#title' => t('Which invoice template do you want to use'),
      '#options' => array(),
      '#default_value' => NULL,
      '#description' => t(
        'To choose a template you must first enter the authentication information above, and establish a connection to MoneyBird.'
      ),
    );
  }

  $form['moneybird_commerce_sync'] = array(
    '#type' => 'fieldset',
    '#collabsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['moneybird_commerce_sync']['moneybird_commerce_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cron should be used for syncing the database.'),
    '#description' => 'If not checked, you may always use the button below to sync the database.',
    '#default_value' => variable_get('moneybird_commerce_cron', ''),
  );

  $form['moneybird_commerce_sync']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Sync the databases manually'),
    '#submit' => array('moneybird_commerce_sync_form_submit'),
  );

  return $form;
}
