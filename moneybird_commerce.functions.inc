<?php
/**
 * @file
 * Module functions for moneybird commerce module.
 * Defines the functions that will be used throughout the module
 * Written by Kevin Muller (kevin@enova-tech.net),
 * Cedric Alfonsi (cedric.alfonsi@enova-tech.net)
 */

/**
 * Return all the invoices contained in moneybird.
 *
 * @return array
 *   Is of type Moneybird\Invoice
 */
function _moneybird_commerce_get_all_invoices() {
  $connector = _moneybird_commerce_init_moneybird_connection();
  $invoice_service = $connector->getService('Invoice');
  $invoices = $invoice_service->getAll();
  return $invoices;
}

/**
 * Creates a Connector to Moneybird.
 *
 * Uses the variables entered in the database.
 *
 * @return \Moneybird\ApiConnector
 *   Returns the Moneybird API connector.
 */
function _moneybird_commerce_init_moneybird_connection() {
  $transport = new Moneybird\HttpClient();
  $transport->setAuth(
    variable_get('moneybird_commerce_emailaddress', ''),
    variable_get('moneybird_commerce_password', '')
  );
  $connector = new Moneybird\ApiConnector(
    variable_get('moneybird_commerce_client', ''),
    $transport,
    new Moneybird\XmlMapper()
  );
  return $connector;
}

/**
 * Return the last commerce customer profile from a UID.
 *
 * @param int $uid
 *   The users ID.
 *
 * @return object
 *   Is of type commerce_customer_profile
 */
function _moneybird_commerce_get_last_commerce_customer_profile_from_uid($uid) {
  $profile_id = db_query(
    'SELECT profile_id FROM {commerce_customer_profile} ccp
     WHERE ccp.uid = ' . $uid . '
       ORDER BY changed DESC'
  )->fetchField();
  $profile = commerce_customer_profile_load($profile_id);
  return $profile;
}

/**
 * Create a MoneyBird Invoice from a Commerce order.
 *
 * This function doesn't save the invoice. Only returns the object.
 *
 * @param int $order_id
 *   The order ID.
 *
 * @return bool|\Moneybird\Invoice
 *   FALSE if it didn't work or the Moneybird Invoice object.
 */
function _moneybird_commerce_get_mb_invoice_from_order($order_id) {
  $order = commerce_order_load($order_id);
  $details = new Moneybird\Invoice_Detail_Array();
  $profile = _moneybird_commerce_get_last_commerce_customer_profile_from_uid($order->uid);
  $profile = $profile->commerce_customer_address['und'][0];
  $connector = _moneybird_commerce_init_moneybird_connection();

  foreach ($order->commerce_line_items['und'] as $k => $line_item) {
    $li = commerce_line_item_load($line_item['line_item_id']);

    $product_label = '';
    if (isset($li->commerce_product[LANGUAGE_NONE][0]['product_id']) && !empty($li->commerce_product[LANGUAGE_NONE][0]['product_id'])) {
      $product = commerce_product_load($li->commerce_product[LANGUAGE_NONE][0]['product_id']);
      $product_label = $product->title;
    }

    if (empty($product_label)) {
      $product_label = $li->line_item_label;
    }

    $details->append(
      new Moneybird\Invoice_Detail(
        array(
          'amount' => $li->quantity,
          'description' => $product_label,
          'price' => $li->commerce_unit_price['und'][0]['amount'] / 100,
        )
      )
    );
  }

  $mid = db_query(
    'SELECT mid FROM {moneybird_users} mu
     WHERE mu.uid =' . $order->uid
  )->fetchField();

  // If we cannot retrieve the MoneyBird Id from db (anonymous user), we
  // retrieve it from the session (it was placed there after saving).
  if (!$mid) {
    if (isset($_SESSION['moneybird_data']['contact_id'])) {
      $mid = $_SESSION['moneybird_data']['contact_id'];
    }
    else {
      throw new Exception(t('Impossible to retrieve contact id'));
    }
  }

  $contact = new Moneybird\Contact(
    array('id' => $mid)
  );

  $name = _mb_split_full_name($profile['name_line']);
  $invoice = new Moneybird\Invoice(
    array(
      'details' => $details,
      'lastname' => $name['last_name'],
      'pricesAreInclTax' => variable_get('moneybird_commerce_price_incl_taxes', 0),
      'invoiceProfileId' => variable_get('moneybird_commerce_template'),
    ),
    $contact
  );
  return $invoice;
}

/**
 * Synchronize the contacts from Drupal to Moneybird.
 *
 * Basically search for the users that have no matching
 * in moneybird, then create the corresponding contact
 *
 * @param int $nb_sync
 *   Returns the number of contacts that have been synced
 *
 * @return bool
 *   TRUE if there were no contacts saved or the saving was successfull. FALSE
 *   otherwise.
 */
function _moneybird_commerce_sync_drupal_to_moneybird_contacts(&$nb_sync = 0) {
  $nb_sync = 0;

  $result = db_query(
    'SELECT uid FROM {moneybird_users} mu
     WHERE mu.mid IS NULL'
  )->fetchAll();

  if (!$result) {
    return TRUE;
  }

  foreach ($result as $r) {
    $profile = _moneybird_commerce_get_last_commerce_customer_profile_from_uid($r->uid);
    $profile = $profile->commerce_customer_address['und'][0];
    $user = user_load($r->uid);

    // Now, either update the customer profile, or save a new one if it doesn't
    // exist.
    $name = _mb_split_full_name($profile['name_line']);
    $connector = _moneybird_commerce_init_moneybird_connection();
    $contact_service = $connector->getService('Contact');
    $contact = new Moneybird\Contact();
    $contact->setData(
      array(
        'company_name' => $profile['organisation_name'],
        'firstname' => $name['first_name'],
        'lastname' => $name['last_name'],
        'address1' => $profile['thoroughfare'],
        'address2' => $profile['premise'],
        'zipcode' => $profile['postal_code'],
        'city' => $profile['locality'],
        'country' => $profile['country'],
        'email' => $user->mail,
        // 'phone' => $arg->billing_phone, // Not available in commerce ?,
      )
    );
    try {
      $contact->save($contact_service);
    } catch (Exception $e) {
      // If we cannot save the contact for some reasons, we just return. We will
      // try later.
      return FALSE;
    }

    // Update the contact mid in the table. It has now been synchronized.
    $data = array('uid' => $r->uid, 'mid' => $contact->id);
    drupal_write_record('moneybird_users', $data, 'uid');
    $nb_sync++;
  }
  return TRUE;
}

/**
 * Synchronize the orders from Drupal to Moneybird.
 *
 * Basically search for the orders that have no matching in Moneybird, then
 * create the corresponding invoice
 *
 * @param int $nb_sync
 *   Returns the number of invoices that have been synced.
 *
 * @return bool
 *   TRUE if there were no orders saved or the saving was successfull. FALSE
 *   otherwise.
 */
function _moneybird_commerce_sync_drupal_to_moneybird_invoices(&$nb_sync = 0) {
  $nb_sync = 0;

  $result = db_query(
    'SELECT id FROM {moneybird_orders} mo
     WHERE mo.mid IS NULL'
  )->fetchAll();

  if (!$result) {
    return TRUE;
  }

  foreach ($result as $r) {
    $order = commerce_order_load($r->id);
    // $profile = $profile->commerce_customer_address['und'][0];
    $user = user_load($order->uid);

    // Now, either update the customer profile, or save a new one if it doesn't
    // exist.
    $connector = _moneybird_commerce_init_moneybird_connection();
    $invoice = _moneybird_commerce_get_mb_invoice_from_order($order->order_id);
    $invoice_service = $connector->getService('Invoice');
    try {
      $invoice->save($invoice_service);
    } catch (Exception $e) {
      // If we cannot save the contact for some reasons, we just return. We will
      // try later.
      return FALSE;
    }

    // Update the contact mid in the table. It has now been synchronized.
    $data = array('id' => $r->id, 'mid' => $invoice->id);
    drupal_write_record('moneybird_orders', $data, 'id');
    $nb_sync++;
  }
  return TRUE;
}

/**
 * Split a full name into a first name and a last name.
 *
 * @param string $full_name
 *   The full name that needs to be split.
 *
 * @return array
 *   With indexes "first_name" and "last_name".
 */
function _mb_split_full_name($full_name) {
  $results = array();

  $r = explode(' ', $full_name);
  $size = count($r);

  // Check first for period, assume salutation if so.
  if (mb_strpos($r[0], '.') === FALSE) {
    $results['salutation'] = '';
    $results['first_name'] = $r[0];
  }
  else {
    $results['salutation'] = $r[0];
    $results['first_name'] = $r[1];
  }

  // Check last for period, assume suffix if so.
  if (mb_strpos($r[$size - 1], '.') === FALSE) {
    $results['suffix'] = '';
  }
  else {
    $results['suffix'] = $r[$size - 1];
  }

  // Combine remains into last.
  $start = ($results['salutation']) ? 2 : 1;
  $end = ($results['suffix']) ? $size - 2 : $size - 1;

  $last = '';
  for ($i = $start; $i <= $end; $i++) {
    $last .= ' ' . $r[$i];
  }
  $results['last_name'] = trim($last);

  return $results;
}
